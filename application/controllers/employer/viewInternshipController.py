from application import app
from flask import abort

# Use individual models as:
# s = models.Student()
# assuming Student is a class in studentModel.py
from application.models import *
from application.models.internshipModel import Internship 
from application.config import *
from application.logic.validation import check_session
from application.logic.validation import getUserFromToken


from flask import \
    session, \
    render_template, \
    request, \
    url_for, \
    session

# PURPOSE: Lets an employer view their posted internship.
@app.route('/employer/viewInternship/', methods = ['GET'])
@check_session()
def viewInternship():
  print "This is the session email {}".format(session['email'])
  print "This is the session token {}".format(session['token'])
  #returns a user object
  user=getUserFromToken(session['token'])
  internships = Internship.select().where(Internship.created_by == user.UID)                                         

  return render_template("views/employer/viewInternshipView.html", config        = config,
                                                                   navBarConfig  = navBarConfig,
                                                                   internships    = internships)

