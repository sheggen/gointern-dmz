from application import app

# Use individual models as:
# s = models.Student()
# assuming Student is a class in studentModel.py
from datetime import datetime
from application.models import *
from application.models.internshipModel import Internship
from application.models.supervisorModel import Supervisor
from application.models.organizationModel import Organization
from application.models.majortointernshipModel import Majortointernship
from application.logic.validation import check_session
from application.config import *
from application.logic.validation import getUserFromToken
from flask import \
    session, \
    render_template, \
    redirect, \
    request, \
    url_for

# PURPOSE: Lets an employer post an internship.
@app.route('/employer/postInternship', methods = ['GET', 'POST']) 
@check_session()
def postInternship():
    if request.method=="GET":
        user=getUserFromToken(session['token'])
        return render_template("views/employer/postInternshipView.html", config    = config,
                                                                     navBarConfig  = navBarConfig,
                                                                            user   = user)
    if request.method=="POST":
        data=request.form
        user=getUserFromToken(session['token'])
        duplicatesNumber=1

        if data['duplicatesNumber'] != "":
            duplicatesNumber=int(data['duplicatesNumber'])
        for x in range(duplicatesNumber):
            new_internship=Internship.create(title=data['title'],
                                             description=data['description'],
                                             location=data['location'],
                                             deadline= datetime.strptime(data['deadline'], "%m/%d/%Y %I:%M %p") if data['deadline'] != "" else "",
                                             start_date=datetime.strptime(data['start_date'], "%m/%d/%Y") if data['start_date'] != "" else "", 
                                             other_stipend=data['other_stipend'],
                                             created_by=user.UID)
            if data['duration'] != "":
                new_internship.duration=data['duration']
            if new_internship.title == "":
                new_internship.title = "Untitled"
                
            c_s= request.form.getlist('class_standing')
            new_internship.class_standing=', '.join(c_s)
            new_internship.hourly_rate=""
            new_internship.compensation=""
            if data['useMeHidden'] == "0":
                new_internship.useMe = False
            if data['hiddenOne']:
                new_internship.hourly_rate=data['hourly_rate']
                new_internship.compensation=data['compensation']
                new_internship.is_paid=True
            print data['org_type']
            org=Organization.create(name=data['org_name'],
                                    url=data['org_url'],
                                    description=data['org_description'],
                                    org_type=data['org_type'],
                                    address1=data['address1'],
                                    address2=data['address2'],
                                    city=data['city'],
                                    state=data['state'],
                                    zip_code=data['zip_code'],
                                    country=data['country'])
                                    
            supervisor=Supervisor.create(first_name=data['first_name'],
                                         last_name=data['last_name'],
                                         phone=data['supervisor_phone'],
                                         email=data['supervisor_email'])
            new_internship.organization=org.OID
            new_internship.supervisor=supervisor.SID
            if data['button']=="save":
                new_internship.status=0
            elif data['button']=="submit":
                new_internship.status=1
            new_internship.save()
        return redirect("employer/viewInternship")
                                                                     