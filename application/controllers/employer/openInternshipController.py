from application import app
from application.models import *
from application.config import *
from application.models.internshipModel import Internship
from application.models.organizationModel import Organization
from application.logic.validation import check_session
from application.models.supervisorModel import Supervisor
from datetime import datetime, date
from application.logic.validation import getUserFromToken

from flask import \
    render_template, \
    session, \
    request, \
    url_for, \
    Markup, \
    redirect

# PURPOSE: Open a certain activity
@app.route('/employer/open/<int:IID>/', methods = ['GET', 'POST'])
@check_session()
def openInternship(IID):
    if request.method == "GET":
      internship = Internship.get(Internship.IID == IID)
      
      """ 
        Turn the unicode into a markup object to have it render as HTML
        http://flask.pocoo.org/docs/0.11/templating/#controlling-autoescaping
      """
      user=getUserFromToken(session['token'])
      description=""
      
      if internship.description is not None:
        description = Markup(internship.description)
      org_description=""
      if internship.organization.description is not None:
        org_description = Markup(internship.organization.description)
        
      checkbox=0
      useMeCheckbox=1
      
      if internship.useMe == False:
        useMeCheckbox = 0
      if internship.is_paid == True:
          checkbox=1
      print internship.start_date
      # if internship.start_date == None:
      #   internship.start_date = ""
      # we should be doing this in the template
      return render_template( "views/employer/showInternshipView.html",
                              config = config,
                              internship=internship,
                              description=description,
                              org_description = org_description,
                              checkbox = checkbox,
                              useMeCheckbox = useMeCheckbox,
                              user  = user
                            )
    else:
      data=request.form
      internship = Internship.get(Internship.IID == IID)
      if data['button']=="delete":
        internship.is_deleted=True
        internship.save()
      else:      
        if internship.title == "":
                internship.title = "Untitled"
        else:
                internship.title=data['title']
        print data['deadline']
        internship.description=data['description']
        internship.location=data['location']
        internship.deadline=datetime.strptime(data['deadline'], "%m/%d/%Y %I:%M %p")
        if data['duration'] != "":
          internship.duration=data['duration']
        print data['start_date']
        if data['start_date'] != "":
          internship.start_date=datetime.strptime(data['start_date'], "%m/%d/%Y")
        internship.other_stipend=data['other_stipend']
        internship.organization.name=data['org_name']
        internship.organization.url=data['org_url']
        internship.organization.description=data['org_description']
        internship.organization.org_type=data['org_type']
        internship.organization.address1=data['address1']
        internship.organization.address2=data['address2']
        internship.organization.city=data['city']
        internship.organization.state=data['state']
        internship.organization.zip_code=data['zip_code']
        internship.organization.country=data['country']
        internship.supervisor.first_name=data['first_name']
        internship.supervisor.last_name=data['last_name']
        internship.supervisor.phone=data['supervisor_phone']
        internship.supervisor.email=data['supervisor_email']
        internship.is_paid=False
        internship.hourly_rate=""
        internship.compensation=""
        internship.useMe=True
        if data['useMeHidden'] == "0":
          internship.useMe=False
        if data['hiddenOne'] == "1":
           internship.hourly_rate=data['hourly_rate']
           internship.compensation=data['compensation']
           internship.is_paid=True
        
        c_s= request.form.getlist('class_standing')
        internship.class_standing=', '.join(c_s) 
        
        if data['button']=="save":
          internship.status=0
        elif data['button']=="submit":
          internship.status=1
        
        internship.save()
        internship.organization.save()
        internship.supervisor.save()
        
      return redirect("employer/viewInternship")
