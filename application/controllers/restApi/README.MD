# GoIntern DMZ API DOCUMENTATION
----

**Show Internships**
----
  Returns a JSON, which is an array of Internship objects

* **URL**

  /api/v1/internships

* **Method:**
  
  `GET`
  
*  **URL Params**

   **Required:**
 
   None

   **Optional:**
 
   Maybe in the future? Class, Location, etc

* **Success Response:**

  * **Code:** 200 <br />
    **Content-Type:** application/json <br />
    **Content:** `[Internship Objects]`
 
* **Error Response:**

  * **Code:** 404 NOT FOUND <br />
    **TODO:** Think about what error to return

* **Notes:**

**Show Internship**
----
  Returns a JSON, which is an Internship object

* **URL**

  /api/v1/internship/:id

* **Method:**
  
  `GET`
  
*  **URL Params**

   **Required:**
 
   id=[Integer]

   **Optional:**
 
   None

* **Success Response:**

  * **Code:** 200 <br />
    **Content-Type:** application/json <br />
    **Content:** Internship Object
 
* **Error Response:**

  * **Code:** 404 NOT FOUND <br />
    **TODO:** Think about what error to return

* **Notes:**

**Show Active Internships**
----
  Returns a JSON, which is an array of Internship objects who's deadline has not passed.

* **URL**

  /api/v1/internships/active

* **Method:**
  
  `GET`
  
*  **URL Params**

   **Required:**
 
   None

   **Optional:**
 
   Maybe in the future? Class, Location, etc

* **Success Response:**

  * **Code:** 200 <br />
    **Content-Type:** application/json <br />
    **Content:** `[Internship Objects]`
 
* **Error Response:**

  * **Code:** 404 NOT FOUND <br />
    **TODO:** Think about what error to return

* **Notes:**