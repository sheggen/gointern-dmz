import glob, os

apis = []
directoryOfThisFile = os.path.dirname(os.path.realpath(__file__))
for file in glob.glob(directoryOfThisFile + "/*Api.py"):
    # print "File: {0}".format(file)
    apis.append(os.path.splitext(os.path.basename(file))[0])
    
__all__ = apis