from application import app
from flask import abort, jsonify
from playhouse.shortcuts import model_to_dict, dict_to_model
from application.models import *
from application.models.internshipModel import Internship 
from application.config import *
from application.logic.validation import check_session
from application.logic.validation import getUserFromToken
from datetime import datetime


from flask import \
    session, \
    render_template, \
    request, \
    url_for, \
    session

# PURPOSE: Lets an employer view their posted internship.
@app.route('/api/v1/internships/', methods = ['GET'])
def getAllInternships():
  """ returns an array of internship objects"""
  #FIXME: The internship status should be == 2 once we create Esther's part
  internships = Internship.select().where(Internship.status == 1)
  internships = [model_to_dict(Internship.get(Internship.IID == internship.IID))\
                    for internship in internships]
  return jsonify(internships)

@app.route('/api/v1/internships/active', methods=['GET'])
def getAllActiveInternships():
  """ returns an array of internship objects whose deadline are active """
  internships = Internship.select()
  print internships.exists()
  internships = [model_to_dict(Internship.get(Internship.IID == internship.IID))\
                    for internship in internships]
  return jsonify(internships)