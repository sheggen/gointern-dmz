from application import app
from application.models import *
from application.config import *
from application.models.internshipModel import Internship
from application.models.organizationModel import Organization
from application.logic.validation import check_session
from application.models.supervisorModel import Supervisor
from datetime import datetime, date
from application.logic.validation import getUserFromToken

from flask import \
    render_template, \
    session, \
    request, \
    url_for, \
    Markup, \
    redirect

# PURPOSE: Open a certain activity
@app.route('/admin/allInternships/', methods = ['GET', 'POST'])
@check_session()
def allInternships():
    internships=Internship.select()
    return render_template( "views/admin/allInternshipsView.html",
                              internships=internships,
                              config    = config)
   