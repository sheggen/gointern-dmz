"""
addInternshipController.py

Created by: Aleksandra Cvetanovska
Date: 05/01/2016

Opens a Add a New Internship Page
so that data from VLAN can be entered in the DMZ database.
It checks whether the encrypted password coming from VLAN is valid.
Adds a new entry to the Internship model based on the post data.

"""
from application import app
from application.models import *
from application.config import *
from application.models.internshipModel import Internship
from application.models.organizationModel import Organization
from application.models.studentModel import Student
from application.logic.validation import check_session
from application.models.supervisorModel import Supervisor
from datetime import datetime, date
from application.models.userModel import User
from application.logic.validation import getUserFromToken
from Crypto.Cipher import AES
import base64

from flask import \
    render_template, \
    session, \
    request, \
    url_for, \
    Markup, \
    redirect

def decrypt(enc, key ):
    unpad = lambda s : s[0:-ord(s[-1])]
    enc = base64.b64decode(enc)
    iv = enc[:16]
    cipher = AES.new(key, AES.MODE_CBC, iv )
    return unpad(cipher.decrypt( enc[16:] ))

# PURPOSE: Open a certain activity

@app.route('/addInternship/<path:username>', methods = ['GET', 'POST'])
def addInternship(username):
    #username = getUsernameFromEnv()
    # student = Student.get(Student.SID == 1)
    # username = student.username

    print("Username is: {0}".format(username))
    user_name="heggens"
    if request.method == "GET":
        state = 0
        password = 'mysecretpassword'
        decrypted = decrypt(username, password)
        if user_name == decrypted:
            return render_template( "views/admin/addInternshipView.html",   state  = state,
                                                                            config = config)
        # else:
        #     print("Hash {0} is not the same".format(decrypted))
        
    if request.method == "POST":
        data=request.form
        t = "oasda"
        user = User.get(token = t)
        new_internship=Internship.create(title=data['title'],
                                         description=data['description'],
                                         location=data['location'],
                                         deadline= datetime.strptime(data['deadline'], "%m/%d/%Y %I:%M %p") if data['deadline'] != "" else "",
                                         start_date=datetime.strptime(data['start_date'], "%m/%d/%Y") if data['start_date'] != "" else "", 
                                         other_stipend=data['other_stipend'],
                                         created_by = user)
        if data['duration'] != "":
                new_internship.duration=data['duration']
        if new_internship.title == "":
                new_internship.title = "Untitled"
        ############
        # FIXME: It still doesn't record the class_standing
        ############
        new_internship.class_standing=""
        new_internship.hourly_rate=""
        new_internship.compensation=""
        new_internship.useMe = False
        if data['hiddenOne']:
                new_internship.hourly_rate=data['hourly_rate']
                new_internship.compensation=data['compensation']
                new_internship.is_paid=True
        org=Organization.create(name=data['org_name'],
                                    url=data['org_url'],
                                    description=data['org_description'],
                                    org_type=data['org_type'],
                                    address1=data['address1'],
                                    address2=data['address2'],
                                    city=data['city'],
                                    state=data['state'],
                                    zip_code=data['zip_code'],
                                    country=data['country'])
                                    
        supervisor=Supervisor.create(first_name=data['first_name'],
                                         last_name=data['last_name'],
                                         phone=data['supervisor_phone'],
                                         email=data['supervisor_email'])
        new_internship.organization=org.OID
        new_internship.supervisor=supervisor.SID
        new_internship.status=1
            
        new_internship.save()
        state = 1
    return render_template( "views/admin/addInternshipView.html",       state = state,
                                                                        config    = config)
