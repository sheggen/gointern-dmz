from application.models.util import *

class Supervisor (Model):
  SID         = PrimaryKeyField()
  first_name  = TextField()
  last_name   = TextField()
  phone       = TextField(null=True)
  email       = TextField()
  

  class Meta:
    database = getDB("dmz")

