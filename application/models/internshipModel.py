from application.models.util import *
from application.models.supervisorModel import Supervisor
from application.models.organizationModel import Organization
from application.models.userModel import User
from application.models.majortointernshipModel import Majortointernship

class Internship (Model):
  IID           = PrimaryKeyField(null=True)
  supervisor    = ForeignKeyField(Supervisor, null=True)
  organization  = ForeignKeyField(Organization, null=True)
  MIID          = ForeignKeyField(Majortointernship, null=True)
  title         = TextField(null=True)
  description   = TextField(null=True)
  location      = TextField(null=True)
  class_standing= TextField(null=True)
  created_by    = ForeignKeyField(User)
  deadline      = DateTimeField(null=True)
  duration      = IntegerField(null=True)
  start_date    = DateTimeField(null=True)
  is_paid       = BooleanField(default=False)
  hourly_rate   = TextField(null=True)
  compensation  = TextField(null=True)
  other_stipend = TextField(null=True)
  useMe         = BooleanField(default=True)
  is_deleted    = BooleanField(default=False)
  status        = IntegerField(null=True) #0 means unsubmitted, 1 unapproved, 2 approved
  

  class Meta:
    database = getDB("dmz")

