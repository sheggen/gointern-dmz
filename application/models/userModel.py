from application.models.util import *

class User (Model):
  UID           = PrimaryKeyField()
  email         = TextField()
  first_name    = TextField()
  last_name     = TextField()
  phone         = TextField(null = True)
  time          = TextField(null = True)
  token         = TextField()
  isAdmin       = BooleanField(default = False)
  

  class Meta:
    database = getDB("dmz")

