from application.models.util import *

class Organization (Model):
  OID          = PrimaryKeyField()
  name         = CharField()
  url          = CharField(null=True)
  description  = CharField(null=True)
  address1     = CharField(null=True)
  address2     = CharField(null=True)
  city         = CharField(null=True)
  state        = CharField(null=True)
  zip_code     = CharField(null=True)
  country      = CharField(null=True)
  org_type     = CharField(null=True)

  class Meta:
    database = getDB("dmz")

